const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
  console.log("Server online on: " + port);
});

app.use('/', express.static('../front-end'))

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_abonament"
});

connection.connect(function (err) {

  console.log("Connected to database!");
  const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER)";
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});

app.post("/abonament", (req, res) => {
  let nume = req.body.nume;
  let prenume = req.body.prenume;
  let telefon = req.body.telefon;
  let email = req.body.email;
  let facebook = req.body.facebook;
  let tipAbonament = req.body.abonament;
  let nrCard = req.body.nrCard;
  let varsta = req.body.varsta;
  let cod = req.body.cod;
  let cvv = req.body.cvv;

  let error = []
  if (nume === '' || prenume === '' || telefon === '' || email === '' || facebook === '' || tipAbonament === '' || nrCard === '' || cvv === '' || varsta === '' || cod === '') {   // a) câmpuri non-empty
    error.push("Unul sau mai multe campuri nu au fost introduse");
    console.log("Unul sau mai multe campuri nu au fost introduse!");
  }
  else {
    if (nume.length < 3 || nume.length > 20) {  // b) minim 3 caractere, maxim 20
      console.log("Nume invalid!");
      error.push("Nume invalid");
    } else if (!nume.match("^[A-Za-z]+$")) {  // c) câmpuri ce permit doar litere
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if (prenume.length < 3 || prenume.length > 20) {   // b) minim 3 caractere, maxim 20
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (!prenume.match("^[A-Za-z]+$")) {  // c) câmpuri ce permit doar litere
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if (telefon.length != 10) {    // d) validare pentru numărul de telefon ( lungime = 10, doar numere)
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    }
    else if (!telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    if (!email.includes("@gmail.com") && !email.includes("@yahoo.com")) {  //e) validare pentru email (formă specifică exemplu@zzz.yyy)
      console.log("Email invalid!");
      error.push("Email invalid!");
    }
    if (!facebook.includes("https://www.facebook.com/")) {  // f) validarea link-ului de facebook
      console.log("Link Facebook invalid!");
      error.push("Link Facebook invalid!");
    }
    if (nrCard.length != 16) {    // g) validare pentru nrCard (lungime = 16, doar numere)
      console.log("Numar card invalid!");
      error.push("Numar card invalid!");
    } else if (!nrCard.match("^[0-9]+$")) {
      console.log("Numarul de card trebuie sa contina doar cifre!");
      error.push("Numarul de card trebuie sa contina doar cifre!");
    }
    if (cvv.length != 3) {     // h) validare pentru cvv (lungime = 3, doar numere)
      console.log("CVV invalid!");
      error.push("CVV invalid!");
    } else if (!cvv.match("^[0-9]+$")) {
      console.log("CVV trebuie sa contina doar cifre!");
      error.push("CVV trebuie sa contina doar cifre!");
    }
    if (varsta.length < 1 || varsta.length > 3) {    // 2) validare pentru varsta ( lungime , doar numere)
      console.log("Varsta trebuie sa fie de 10 cifre!");
      error.push("Varsta trebuie sa fie de 10 cifre!");
    }
    else if (!varsta.match("^[0-9]+$")) {
      console.log("Varsta trebuie sa contina doar cifre!");
      error.push("Varsta trebuie sa contina doar cifre!");
    }
    if (cod.length != 13) {    // 3) validare pentru CNP ( lungime , doar numere)
      console.log("CNP trebuie sa aiba 13 cifre!");
      error.push("CNP trebuie sa aiba 13 cifre!");
    }
    else if (!cod.match("^[0-9]+$")) {
      console.log("CNP trebuie sa contina doar cifre!");
      error.push("CNP trebuie sa contina doar cifre!");
    }
    let an = parseInt(cod.substring(1,3));
    let luna = parseInt(cod.substring(3,5));
    let zi = parseInt(cod.substring(5,7));
    let sex = parseInt(cod.substring(0,1));
    if(sex === 1 || sex === 2)
    {
      an = 1900+an;
      if(2019-an != varsta)
      {
          console.log("Varsta nu coincinde cu cea din buletin!");  //5) Analizați data de naștere și CNP-ul
          error.push("Varsta nu coincinde cu cea din buletin!");
      }
      else if(sex === 6 || sex === 5)
      {
        an=2000+an;
        if(2019-an != varsta)
        {
          console.log("Varsta nu coincinde cu cea din buletin!");
          error.push("Varsta nu coincinde cu cea din buletin!");
        }

      }
    }
  }

  

  if (error.length === 0) {
    const sql = `INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv, varsta, cnp) VALUES ("${nume}", "${prenume}", "${telefon}", "${email}", "${facebook}", "${tipAbonament}", "${nrCard}", ${cvv}," ${varsta}", "${cod}")`
    connection.query(sql, function (err, result) {
      if (err) throw err;
      console.log("Abonament achizitionat!");
    });

    res.status(200).send({
      message: "Abonament achizitionat!"
    });
    console.log(sql);
  } else {
    res.status(500).send(error)
    console.log("Eroare la inserarea in baza de date!");
  }
});